package xyz.task.stacksearch;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;


import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class NetworkFetchTests {
    @Test
    public void useAppContext() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("xyz.task.stacksearch", appContext.getPackageName());
    }

    @Test
    public void resultsFetchedTest() throws Exception {
        JSONObject object = new JSONObject(Utility.fetchFromStackExchange("Java Exception"));
        assertNotNull(object);
        assertTrue(object.has("items"));
    }
}
