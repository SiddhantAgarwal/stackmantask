package xyz.task.stacksearch;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;


import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by siddhant on 18/09/17.
 */

public class DatabaseInteractionTests {
    String queryString = "Java Exception";
    @Test
    public void addingResultsTest() throws Exception {
        JSONObject object = new JSONObject(Utility.fetchFromStackExchange(queryString));
        JSONArray array = object.getJSONArray("items");
        ArrayList<Result> itemList = new ArrayList<>();
        for (int i=0; i<array.length(); i++) {
            itemList.add(new Result(array.getJSONObject(i).getInt("up_vote_count"),
                    array.getJSONObject(i).getString("title")));
        }
        DatabaseHandler databaseHandler = new DatabaseHandler(InstrumentationRegistry.getTargetContext());
        databaseHandler.addResults(itemList, queryString);
        ArrayList<Result> resultsByQuery = databaseHandler.getResultsByQuery(queryString);
        assertNotNull(resultsByQuery);
        assertTrue(resultsByQuery.size() > 0);
    }

    @Test
    public void fetchingResultsTest() throws Exception {
        DatabaseHandler databaseHandler = new DatabaseHandler(InstrumentationRegistry.getTargetContext());
        ArrayList<Result> resultsByQuery = databaseHandler.getResultsByQuery(queryString);
        assertNotNull(resultsByQuery);
        assertTrue(resultsByQuery.size() > 0);
    }

    @Test
    public void fetchingResultsNotFoundTest() throws Exception {
        DatabaseHandler databaseHandler = new DatabaseHandler(InstrumentationRegistry.getTargetContext());
        ArrayList<Result> resultsByQuery = databaseHandler.getResultsByQuery("Random");
        assertNotNull(resultsByQuery);
        assertTrue(resultsByQuery.size() == 0);
    }
}
