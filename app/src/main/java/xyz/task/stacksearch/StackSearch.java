package xyz.task.stacksearch;

import android.app.Application;
import android.os.StrictMode;

/**
 * Created by siddhant on 16/09/17.
 */

public class StackSearch extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()
                .penaltyLog()
                .penaltyDeath()
                .build());
    }
}
