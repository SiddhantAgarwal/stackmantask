package xyz.task.stacksearch;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by siddhant on 16/09/17.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;


    private static final String DATABASE_NAME = "searchResults";


    private static final String TABLE_NAME = "TABLE_RESULT";

    private static final String KEY_ID = "id";
    private static final String KEY_SEARCH_QUERY = "query";
    private static final String KEY_TITLE = "title";
    private static final String KEY_UPVOTES = "upvotes";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_RESULTS_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_SEARCH_QUERY + " TEXT,"
                + KEY_TITLE + " TEXT, " + KEY_UPVOTES + " INTEGER)";
        db.execSQL(CREATE_RESULTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    void addResults(ArrayList<Result> list, String query) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.beginTransaction();

        for(int i=0; i<list.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(KEY_SEARCH_QUERY, query);
            values.put(KEY_TITLE, list.get(i).getTitle());
            values.put(KEY_UPVOTES, list.get(i).getUpvoteCount());

            db.insert(TABLE_NAME, null, values);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }



    public ArrayList<Result> getResultsByQuery(String query) {
        ArrayList<Result> resultArrayList = new ArrayList<Result>();
        String selectQuery = "SELECT " + KEY_TITLE + ", " + KEY_UPVOTES + " FROM " + TABLE_NAME + " WHERE " + KEY_SEARCH_QUERY  + " = '" + query + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor.moveToFirst()) {
            do {
                Result contact = new Result( Integer.parseInt(cursor.getString(1)), cursor.getString(0));
                resultArrayList.add(contact);
            } while (cursor.moveToNext());
        }


        return resultArrayList;
    }

}
