package xyz.task.stacksearch;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Created by siddhant on 16/09/17.
 */

public class ResultsRecylerAdapter extends RecyclerView.Adapter<ResultsRecylerAdapter.ResultViewHolder> {
    private ArrayList<Result> itemList;

    public ResultsRecylerAdapter(ArrayList<Result> list) {
        this.itemList = list;
    }

    @Override
    public ResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_result_item, parent, false);
        return new ResultViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ResultViewHolder holder, int position) {
        Result result = itemList.get(position);
        holder.title.setText(result.getTitle());
        holder.upVotes.setText(Integer.toString(result.getUpvoteCount()));
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ResultViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView upVotes;
        public ResultViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title_textview);
            upVotes = (TextView) itemView.findViewById(R.id.upvotes_textview);
        }
    }
}
