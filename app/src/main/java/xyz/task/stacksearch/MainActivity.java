package xyz.task.stacksearch;

import android.app.SearchManager;
import android.content.Context;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    
    private ArrayList<Result> itemList;
    private RecyclerView resultsRecylerView;
    private ResultsRecylerAdapter resultsAdapter;
    private ProgressBar loadingIndicator;
    private DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        loadingIndicator = (ProgressBar) findViewById(R.id.progress_bar);

        resultsRecylerView = (RecyclerView) findViewById(R.id.results_recycler_view);
        itemList = new ArrayList<>();

        databaseHandler = new DatabaseHandler(this);

        resultsAdapter = new ResultsRecylerAdapter(itemList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        resultsRecylerView.setLayoutManager(layoutManager);
        resultsRecylerView.setItemAnimator(new DefaultItemAnimator());
        resultsRecylerView.setAdapter(resultsAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(
                new SearchView.OnQueryTextListener(){
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        loadingIndicator.setVisibility(View.VISIBLE);
                        resultsRecylerView.setVisibility(View.GONE);
                        ArrayList<Result> resultsByQuery = databaseHandler.getResultsByQuery(query);
                        if(resultsByQuery.size() == 0) {
                            FetchResultsTask task = new FetchResultsTask();
                            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, query);
                        } else {
                            if(itemList != null) {
                                itemList.clear();
                            }
                            for (int i=0; i<resultsByQuery.size(); i++) {
                                itemList.add(resultsByQuery.get(i));
                            }
                            loadingIndicator.setVisibility(View.GONE);
                            resultsRecylerView.setVisibility(View.VISIBLE);
                            resultsAdapter.notifyDataSetChanged();
                        }
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        return false;
                    }
                });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private class FetchResultsTask extends AsyncTask<String, Void, String> {
        private String queryString;
        @Override
        protected String doInBackground(String... params) {
            this.queryString = params[0];
            return Utility.fetchFromStackExchange(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s != null) {
                try {
                    JSONObject result = new JSONObject(s);
                    JSONArray array = result.getJSONArray("items");
                    if(itemList != null) {
                        itemList.clear();
                    }
                    for (int i=0; i<array.length(); i++) {
                        itemList.add(new Result(array.getJSONObject(i).getInt("up_vote_count"),
                                array.getJSONObject(i).getString("title")));
                    }
                    resultsAdapter.notifyDataSetChanged();
                    databaseHandler.addResults(itemList, this.queryString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            loadingIndicator.setVisibility(View.GONE);
            resultsRecylerView.setVisibility(View.VISIBLE);
        }
    }
}
