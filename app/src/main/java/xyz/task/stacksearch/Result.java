package xyz.task.stacksearch;

/**
 * Created by siddhant on 16/09/17.
 */

public class Result {
    private int upvoteCount;
    private String title;

    public Result(int upvoteCount, String title) {
        this.upvoteCount = upvoteCount;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public int getUpvoteCount() {
        return upvoteCount;
    }
}
