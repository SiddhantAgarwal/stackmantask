package xyz.task.stacksearch;



import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;
/**
 * Created by siddhant on 18/09/17.
 */

public class DatabaseTest {

    @Mock
    DatabaseHandler databaseHandler;

    @Test
    public void simpleQuery() throws Exception {
        databaseHandler = mock(DatabaseHandler.class);
        ArrayList<Result> mockList = new ArrayList<>();
        mockList.add(new Result(100, "Java Exception 1"));
        mockList.add(new Result(110, "Java Exception 2"));
        mockList.add(new Result(120, "Java Exception 3"));
        when(databaseHandler.getResultsByQuery("Java Exception")).thenReturn(mockList);
        assertSame(databaseHandler.getResultsByQuery("Java Exception"), mockList);
    }

    @Test
    public void emptyQueryNotNull() throws Exception {
        databaseHandler = mock(DatabaseHandler.class);
        ArrayList<Result> mockList = new ArrayList<>();
        when(databaseHandler.getResultsByQuery("Java Exception")).thenReturn(mockList);
        assertNotNull(databaseHandler.getResultsByQuery("Java Exception"));
    }

    @Test
    public void emptyQueryValid() throws Exception {
        databaseHandler = mock(DatabaseHandler.class);
        ArrayList<Result> mockList = new ArrayList<>();
        when(databaseHandler.getResultsByQuery("Java Exception")).thenReturn(mockList);
        assertTrue(databaseHandler.getResultsByQuery("Java Exception").size() == 0);
    }

}
